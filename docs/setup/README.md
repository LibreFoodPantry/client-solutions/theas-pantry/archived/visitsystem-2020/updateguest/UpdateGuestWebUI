# UpdateGuestWebUI Setup Documentation

## Requirements 

The UpdateGuestWebUI project is an [Angular](https://angular.io/) application and requires the following tools to be installed:

- [Node.js](https://nodejs.org/en/)
- [Angular CLI](https://cli.angular.io/)

## Setup

To run this project, clone it locally on your computer. Open a terminal and navigate to the top-level directory for the project (the UpdateGuestWebUI folder) and enter the command ``npm install`` to install the necessary files and dependencies. Note, this initial setup process may take some time.

The project can then be run by entering the ``ng serve`` command in the terminal. Open a web browser and navigate to localhost:4200 to verify that the project is running correctly.

***This process will continue to run and must be manually closed in the terminal by using ``ctrl+c``***.

---
Copyright &copy; 2020 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.