# UpdateGuestWebUI Developer Documentation

## Front End Architecture 

*Table of Contents*

1. Purpose
2. Front End Architecture Design
    1. Modules
    2. Components
    3. Web UI Design

** **

### Purpose
This document will develop and explain UpdateGuestWebUI's front end architecture. Web UI design and Component explanation will be included.

### Front End Architecture Design

![Frontend Diagram](./FrontendDiagram.png)

#### Modules 
UpdateGuestWebUI will have only one module for the entire application which will be a collection of all components detailed below. All services and database connections will be implemented in this module. This module will interact with other services offered in the platform like RegisterGuest. 

#### Components 
UpdateGuestWebUI is separated into the following components based on what functionality and purpose the component serves in the application.


##### AgeAndEmployment

This component will contain a dropdown selection for age and radio buttons for employment status.

##### DisplayGuest

This component displays a guest's information.

##### HousingSituation 

This component will have radio buttons with resident information and if the guest lives on or off campus.
It will also have two basic inputs for the ZIP Code and the Household Member Numbers. 

##### SupplementalServices 

This component will have several checkboxes with offered Services: Social Security, TANF/EADC, SNAP/Food Stamps, WIC, SFSP, School Breakfast, School Lunch, and Financial Aid.
It will also contain an input for Other Benefit(s).

##### Toolbar

This component will be a top bar with the Worcester State University logo and the name of the page.



#### Web UI Design 

The Web UI design is implemented from a diagram created in a [Wireframe diagram](https://wireframe.cc/Xtlnsx).






---
Copyright &copy; 2020 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.


