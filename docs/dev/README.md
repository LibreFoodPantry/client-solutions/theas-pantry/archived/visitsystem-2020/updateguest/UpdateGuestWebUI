UpdateGuestWebUI Developer Documentation
===============

Docker configuration for UpdateGuestUI is done.

Refer to the Merge Request: [!8](https://gitlab.com/LibreFoodPantry/modules/visitmodule-tp/updateguest/UpdateGuestWebUI/-/merge_requests/8)

### Running container:
- Since this container is configured using NGINX proxy server, by default this container will run at PORT 8080, using the Docker container IP.
- First you need to build the Docker container and name the image:`docker build -t [image-name] . `<br>
Note: (don't forget the "." in the end, the image name will be set)
- Running the container: `docker run --name [container name] -d -p 8080:80 [image-name]` <br>
Note: (--name is used to set the name of the container)<br>
- If you want to rerun the container simply use the command without --name option:<br>
 `docker run -d -p 8080:80 angular-ui`
- Checking if the container is running: `docker container ls`
- Checking for the machine IP where the container runs, run the command: `docker-machine IP`
- To run the project in a web browser you need to hit the server with the specified PORT: `192.182.99.100:8080`

![Server_Running](./Server_running.PNG)

- To run this IP you need to set up a Manual proxy on your computer

![Proxy_Setup](./Proxy_Setup.PNG)

- To restart an existing or old container run: `docker start [container-name]`
- To stop a running container run: `docker stop [container-name]`

### REST call
On submit, changes are sent to the backend through a POST request: http://**localhost**:3000/api/guestUpdates [Located here](../../src/app/app.component.ts) \
**\*This IP may not work for all systems \
This problem is addressed in [issue 50](https://gitlab.com/LibreFoodPantry/modules/visitmodule-tp/updateguest/Plan/-/issues/50)**

Copyright &copy; 2020 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
