# UpdateGuestWebUI Testing Documentation


## Angular tests

Tests for the UpdateGuestWebUI Angular application are written using Jasmine and are run using Karma (see the [Angular testing guide](https://angular.io/guide/testing#testing) for more information on this topic).

These tests help to verify that changes to the input fields in the UI are properly registered and that components render correctly.

Tests are written in the `*.component.spec.ts` file for each Angular component.

To run the tests, open a terminal at the project level and run ``ng test``, the test runner will open in a new browser window, along with the some of the test statuses being displayed in the command prompt.

***This process will continue to run and must be manually closed in the terminal by using ``ctrl+c``***.

---
Copyright &copy; 2020 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.