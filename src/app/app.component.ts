import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { SupplementalServicesComponent } from './supplemental-services/supplemental-services.component';
import { HousingSituationComponent } from './housing-situation/housing-situation.component';
import { AgeAndEmploymentComponent } from './age-and-employment/age-and-employment.component';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  @ViewChild(SupplementalServicesComponent, {static: true}) supplementalServicesForm: SupplementalServicesComponent;
  @ViewChild(HousingSituationComponent, {static: true}) housingSituationForm: HousingSituationComponent;
  @ViewChild(AgeAndEmploymentComponent, {static: true}) ageAndEmploymentForm: AgeAndEmploymentComponent;
  title = 'AngularUI';
  updateForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private http: HttpClient) {}

  ngOnInit() {
    this.updateForm = this.formBuilder.group({
      idNumber: ['', Validators.required],
      supplementalServices: this.supplementalServicesForm.createGroup(),
      housingSituation: this.housingSituationForm.createGroup(),
      ageAndEmployment: this.ageAndEmploymentForm.createGroup()
    });
  }

  submitUpdates() {
    const housingSituationForm = this.updateForm.controls.housingSituation.value;
    const supplementalServicesForm = this.updateForm.controls.supplementalServices.value;

    if (supplementalServicesForm.other_benefits === '') {
      supplementalServicesForm.other_benefits = null;
    }

    const ageAndEmploymentForm = this.ageAndEmploymentForm.value;

    this.http.post<any>('http://localhost:3000/api/guestUpdates', {
      id: this.updateForm.controls.idNumber.value,
      ageRange: ageAndEmploymentForm.ageRange,
      isResident: housingSituationForm.housingSituation,
      zipCode: housingSituationForm.zipCode,
      socialSecurity: supplementalServicesForm.social_security,
      tanfeadc: supplementalServicesForm.tanf_eadc,
      snap: supplementalServicesForm.snap_food_stamps,
      wic: supplementalServicesForm.wic,
      sfsp: supplementalServicesForm.sfsp,
      schoolBreakfast: supplementalServicesForm.school_breakfast,
      schoolLunch: supplementalServicesForm.school_lunch,
      financialAid: supplementalServicesForm.financial_aid,
      otherBenefit: supplementalServicesForm.other_benefits,
      employed: ageAndEmploymentForm.status,
      householdSize: housingSituationForm.numHouseholdMembers
    }).subscribe(data => console.log(data));
  }

  cancel() {
    console.log('Cancel button clicked');
  }

}
