import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,} from '@angular/forms';

 export interface AgeAndEmploymentValue {
   ageRange: string;
   status: boolean;
 }

@Component({
  selector: 'app-age-and-employment',
  templateUrl: './age-and-employment.component.html',
  styleUrls: ['./age-and-employment.component.css'],
})

export class AgeAndEmploymentComponent implements OnInit {
  ageAndEmploymentGroup: FormGroup;

  Range: any = ['0-4','5-17','18-64','65+']

  get value(): AgeAndEmploymentValue{
    return this.ageAndEmploymentGroup.value;
  }


  constructor( private formBuilder: FormBuilder) {}

  ngOnInit(){}

  createGroup(){
    this.ageAndEmploymentGroup = this.formBuilder.group({
      ageRange: [],
      status: []
    });

    return this.ageAndEmploymentGroup;
  }
  
}
