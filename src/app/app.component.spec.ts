import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import {
  MatCheckboxModule,
  MatInputModule,
  MatExpansionModule,
  MatRadioModule,
  MatSelectModule,
  MatDividerModule,
  MatButtonModule,
  MatToolbarModule,
  MAT_CHECKBOX_CLICK_ACTION
} from '@angular/material';
import { AppComponent } from './app.component';

import { ReactiveFormsModule } from '@angular/forms';
import { SupplementalServicesComponent } from './supplemental-services/supplemental-services.component';
import { HousingSituationComponent } from './housing-situation/housing-situation.component';
import { AgeAndEmploymentComponent } from './age-and-employment/age-and-employment.component';
import { DisplayGuestComponent } from './display-guest/display-guest.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        SupplementalServicesComponent,
        HousingSituationComponent,
        AgeAndEmploymentComponent,
        DisplayGuestComponent,
        ToolbarComponent
      ],
      imports: [
        ReactiveFormsModule,
        BrowserModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatExpansionModule,
        MatCheckboxModule,
        MatRadioModule,
        MatSelectModule,
        MatDividerModule,
        MatButtonModule,
        MatToolbarModule,
        HttpClientModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'AngularUI'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('AngularUI');
  });

  it('should update the id number input', () => {
    const input = fixture.nativeElement.querySelector('#idNumber');

    input.value = 9263;
    input.dispatchEvent(new Event('input'));

    expect(fixture.componentInstance.updateForm.controls['idNumber'].value).toEqual(9263);

  });

  it('should call submit function when submit updates button clicked', () => {
    let submitUpdatesSpy : jasmine.Spy;
    const saveButton = fixture.nativeElement.querySelector('#submitUpdates');
    submitUpdatesSpy = spyOn(component, 'submitUpdates');

    saveButton.click();

    expect(submitUpdatesSpy.calls.any()).toBe(true, 'Component.submitUpdates called');
  })

  it('should call cancel function when cancel button clicked', () => {
    let cancelSpy : jasmine.Spy;
    const cancelButton = fixture.nativeElement.querySelector('#cancel');
    cancelSpy = spyOn(component, 'cancel');

    cancelButton.click();

    expect(cancelSpy.calls.any()).toBe(true, 'Component.cancel called');
  })

});
