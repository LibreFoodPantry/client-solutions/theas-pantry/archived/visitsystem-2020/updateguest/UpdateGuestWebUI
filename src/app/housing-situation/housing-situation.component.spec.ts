import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HousingSituationComponent } from './housing-situation.component';
import { AppComponent } from '../app.component';
import { SupplementalServicesComponent } from '../supplemental-services/supplemental-services.component';
import { AgeAndEmploymentComponent } from '../age-and-employment/age-and-employment.component';
import { DisplayGuestComponent } from '../display-guest/display-guest.component';
import { ToolbarComponent } from '../toolbar/toolbar.component';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDividerModule,
  MatExpansionModule,
  MatRadioModule,
  MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

describe('HousingSituationComponent', () => {
  let component: HousingSituationComponent;
  let fixture: ComponentFixture<HousingSituationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        SupplementalServicesComponent,
        HousingSituationComponent,
        AgeAndEmploymentComponent,
        DisplayGuestComponent,
        ToolbarComponent
      ],
      imports: [
        ReactiveFormsModule,
        BrowserModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatExpansionModule,
        MatCheckboxModule,
        MatRadioModule,
        MatSelectModule,
        MatDividerModule,
        MatButtonModule,
        MatToolbarModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HousingSituationComponent);
    component = fixture.componentInstance;
    component.createGroup();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
