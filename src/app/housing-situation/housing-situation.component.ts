import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

@Component({
  selector: 'app-housing-situation',
  templateUrl: './housing-situation.component.html',
  styleUrls: ['./housing-situation.component.css']
})
export class HousingSituationComponent implements OnInit {

  housingSituationGroup: FormGroup;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {}

  createGroup() {
    this.housingSituationGroup = this.formBuilder.group({
      housingSituation: [, [Validators.required]],
      zipCode: [, [Validators.required]],
      numHouseholdMembers: [, Validators.required]
    });

    return this.housingSituationGroup;
  }
}
