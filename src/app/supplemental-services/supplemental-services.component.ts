import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-supplemental-services',
  templateUrl: './supplemental-services.component.html',
  styleUrls: ['./supplemental-services.component.css']
})
export class SupplementalServicesComponent implements OnInit {
  supplementalServicesGroup: FormGroup

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    
  }

  createGroup() {
    this.supplementalServicesGroup = this.formBuilder.group({
      social_security: [, [Validators.required]],
      tanf_eadc: [, [Validators.required]],
      snap_food_stamps: [, [Validators.required]],
      wic: [, [Validators.required]],
      sfsp: [, [Validators.required]],
      school_breakfast: [, [Validators.required]],
      school_lunch: [, [Validators.required]],
      financial_aid: [, [Validators.required]],
      other_benefits: ['', [Validators.required]]
    });

    return this.supplementalServicesGroup;
  }
}
