import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplementalServicesComponent } from './supplemental-services.component';
import { AppComponent } from '../app.component';
import { HousingSituationComponent } from '../housing-situation/housing-situation.component';
import { AgeAndEmploymentComponent } from '../age-and-employment/age-and-employment.component';
import { DisplayGuestComponent } from '../display-guest/display-guest.component';
import { ToolbarComponent } from '../toolbar/toolbar.component';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDividerModule,
  MatExpansionModule,
  MatRadioModule,
  MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

describe('SupplementalServicesComponent', () => {
  let component: SupplementalServicesComponent;
  let fixture: ComponentFixture<SupplementalServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        SupplementalServicesComponent,
        HousingSituationComponent,
        AgeAndEmploymentComponent,
        DisplayGuestComponent,
        ToolbarComponent
      ],
      imports: [
        ReactiveFormsModule,
        BrowserModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatExpansionModule,
        MatCheckboxModule,
        MatRadioModule,
        MatSelectModule,
        MatDividerModule,
        MatButtonModule,
        MatToolbarModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplementalServicesComponent);
    component = fixture.componentInstance;
    component.createGroup();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create form', () => {
    const form = fixture.debugElement.componentInstance;
    expect(form).toBeTruthy();
  });

  it('should change Other Benefits value', () => {
    const input = fixture.debugElement.nativeElement.querySelector('#other_benefits');
    input.value = 'Test Benefit';
    input.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(fixture.componentInstance.supplementalServicesGroup.value.other_benefits).toBe('Test Benefit');
  });
});
