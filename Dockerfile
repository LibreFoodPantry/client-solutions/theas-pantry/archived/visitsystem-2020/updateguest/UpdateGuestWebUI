#Stage 1 - Container setup
FROM node:12.7-alpine AS build
RUN mkdir -p /docker/app
WORKDIR /docker/app
COPY package.json /docker/app/package.json
RUN npm install

ENV PATH /docker/app/node_modules/.bin:$PATH
COPY . /docker/app
RUN ng build --prod

#Stage 2 - NGINX setup (by default ) 
FROM nginx:1.17.1-alpine
COPY --from=build /docker/app/dist/AngularUI /usr/share/nginx/html

